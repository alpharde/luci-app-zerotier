a=Map("zerotier",translate("Zerotier"),translate("Zerotier is an open source, cross-platform VPN"))
a:section(SimpleSection).template  = "zerotier/zerotier_status"

t=a:section(NamedSection,"sample_config","zerotier")
t.anonymous=true
t.addremove=false

e=t:option(Flag,"enabled",translate("Enable"))
e.default=0
e.rmempty=false

e=t:option(DynamicList,"join",translate('Zerotier NetID'))
e.password=true
e.rmempty=false

e=t:option(DummyValue,"opennewwindow" , translate("<input type=\"button\" class=\"cbi-button cbi-button-apply\" value=\"Manage networks\" onclick=\"window.open('https://my.zerotier.com/network')\" />"))

return a
